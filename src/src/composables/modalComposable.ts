import { ref } from "vue";

export function modalComposable() {
  const opened = ref(false);
  const modalType = ref<string | null>(null);
  const selectedModelItem = ref(null);

  function openModal(modalItem: any = null, type = "") {
    opened.value = true;
    selectedModelItem.value = modalItem;
    document.body.style.overflow = "hidden";
    modalType.value = type;
  }

  function closeModal(
    clearObject: () => void = () => {
      return;
    }
  ) {
    opened.value = false;
    selectedModelItem.value = null;
    document.body.style.overflow = "auto";
    modalType.value = null;
    clearObject();
  }

  return { opened, selectedModelItem, modalType, openModal, closeModal };
}
